package com.example.tanvi.architecturecomponents.notesitems;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.tanvi.architecturecomponents.database.NoteDatabase;
import com.example.tanvi.architecturecomponents.database.NoteModel;

import java.util.List;

public class NoteListViewModel extends AndroidViewModel {

    private final LiveData<List<NoteModel>> noteList;

    private NoteDatabase noteDatabase;


    public NoteListViewModel(@NonNull Application application) {
        super(application);

        noteDatabase = NoteDatabase.getDatabase(this.getApplication());
        noteList = noteDatabase.noteItemAndNotesModel().getAllNotes();
    }

    public LiveData<List<NoteModel>> getNoteList() {
        return noteList;
    }

    public void deleteNote(NoteModel model){
        new deleteAsyncTask(noteDatabase).execute(model);
    }

    private class deleteAsyncTask extends AsyncTask<NoteModel,Void,Void>{
        private NoteDatabase noteDatabase;

        deleteAsyncTask(NoteDatabase noteDatabase){
            this.noteDatabase = noteDatabase;
        }

        @Override
        protected Void doInBackground(NoteModel... noteModels) {
            noteDatabase.noteItemAndNotesModel().deleteNote(noteModels[0]);
            return null;
        }
    }
}
