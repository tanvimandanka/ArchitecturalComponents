package com.example.tanvi.architecturecomponents.notesitems;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.tanvi.architecturecomponents.database.NoteDatabase;
import com.example.tanvi.architecturecomponents.database.NoteModel;

public class AddNoteViewModel extends AndroidViewModel {

    private NoteDatabase noteDatabase;


    public AddNoteViewModel(@NonNull Application application) {
        super(application);

        noteDatabase = NoteDatabase.getDatabase(this.getApplication());
    }

    public void addNote(final NoteModel noteModel){
        new addAsyncTask(noteDatabase).execute(noteModel);
    }

    private class addAsyncTask extends AsyncTask<NoteModel,Void,Void>{
        private NoteDatabase database;
        public addAsyncTask(NoteDatabase noteDatabase) {
            database = noteDatabase;
        }

        @Override
        protected Void doInBackground(NoteModel... noteModels) {
            database.noteItemAndNotesModel().insertNote(noteModels[0]);
            return null;
        }
    }
}
