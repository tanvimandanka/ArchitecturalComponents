package com.example.tanvi.architecturecomponents.notesitems;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tanvi.architecturecomponents.R;
import com.example.tanvi.architecturecomponents.database.NoteModel;

import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {

    private List<NoteModel> noteList;
    private View.OnLongClickListener onLongClickListener;

    public NoteAdapter(List<NoteModel> noteList, View.OnLongClickListener onLongClickListener) {
        this.noteList = noteList;
        this.onLongClickListener = onLongClickListener;
    }


    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return  new NoteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.note_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        NoteModel noteModel = noteList.get(position);
        holder.noteTitle.setText(noteModel.getNoteTitle());
        holder.noteDescription.setText(noteModel.getNoteDescription());
        holder.itemView.setTag(noteModel);
        holder.itemView.setOnLongClickListener(onLongClickListener);
    }

    public void addNote(List<NoteModel> noteList){
        this.noteList = noteList;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return noteList.size();
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder {
        TextView noteTitle,noteDescription;
        public NoteViewHolder(View itemView) {
            super(itemView);

            noteTitle = itemView.findViewById(R.id.noteTitle);
            noteDescription = itemView.findViewById(R.id.noteDescription);
        }
    }
}
