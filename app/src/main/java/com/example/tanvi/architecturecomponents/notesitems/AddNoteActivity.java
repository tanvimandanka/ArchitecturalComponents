package com.example.tanvi.architecturecomponents.notesitems;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.tanvi.architecturecomponents.R;
import com.example.tanvi.architecturecomponents.database.NoteModel;

public class AddNoteActivity extends AppCompatActivity{
    private EditText noteTitle,noteDescription;
    private Button save;
    private AddNoteViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        noteTitle = findViewById(R.id.edt_title);
        noteDescription = findViewById(R.id.edt_description);
        save = findViewById(R.id.save);

        viewModel = ViewModelProviders.of(this).get(AddNoteViewModel.class);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (noteTitle.getText()==null || noteDescription.getText()==null){
                    return;
                }
                NoteModel noteModel = new NoteModel(noteTitle.getText().toString(),noteDescription.getText().toString());
                viewModel.addNote(noteModel);
                finish();
            }
        });
    }
}
